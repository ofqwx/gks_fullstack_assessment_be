// =======================
// get the packages we need
// =======================
const express = require('express');
const http = require('http');
const endPoints = require('./controllers/index.js');

const app = express();

app.use(endPoints);

const server = http.createServer(app);
server.listen(3001, 'localhost', () => {
  console.log('Transactions API development mode is running on http://localhost:3001');
});
module.exports = {
  app,
  server,
};

