const express = require('express');
const cors = require('cors');
const mock = require('../data/mock.json');

/**
 * Transactions Routes or Endpoints.
 * In this assessment, data is mocked so endpoints just find info in a mock file,
 * usually we call database or another service here.
 */

const endPoints = express.Router();

/** Get all available transactions ids */
endPoints.get('/tx', cors(), (req, res) => {
  const txIds = mock.map((tx) => {
    return tx[Object.keys(tx)[0]].key.transactionID;
  });
  res.status(200).json(txIds);
});

/** Get transaction by id */
endPoints.get('/tx/:transactionId', cors(), (req, res) => {
  const txId = req.params.transactionId;
  let tx = mock.filter((t) => {
    return t[Object.keys(t)[0]].key.transactionID === txId;
  });
  /** If the transaction was found we response a 200 status code with the data. */
  if (tx.length > 0) {
    tx = tx[0][Object.keys(tx[0])[0]];
    /** I delay the response to simulate latency and check ux behavior */
    setTimeout(() => {
      res.status(200).json(tx);
    }, 1000);
  } else {
    /** If the id is misspelled we handle this with a 400 status code response. */
    res.status(400).json('Transaction not found');
  }
});

module.exports = endPoints;
