# README #

### GK Software fullstack assessment Backend ###

## Pre Requirements

* Node JS v8.11.1
* Npm v5.6.0

## Getting Started

Install dependencies
```
cd gks_fullstack_assessment_be
npm install
```

Run server
```
npm start
```

Then call any of this endpoints:

```
http://localhost:3001/tx
http://localhost:3001/tx/[transactionId]
```

## Built With

* [Node JS](http://nodejs.org)
* [Express js](https://expressjs.com)

### Contact ###

* Antonio Aznarez
* antonioaznarez@gmail.com