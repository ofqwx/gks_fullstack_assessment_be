const express = require('express');

/**
 * Index of all controllers (endpoints)
 * this is very helpful if we have many endpoints and we like (I like) to divide them in different files, 
 * in this case we just have one for the moment.
 */

const endPoints = express.Router();

endPoints.use(require('./transactions.js'));

module.exports = endPoints;